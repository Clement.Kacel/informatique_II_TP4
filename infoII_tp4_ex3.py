class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        tuple_insert = False
        for i in range(len(self.items)):
           
            # SI prio élément en cours >> prio item en parametre ALORS on insère élément en cours
            if self.items[i][0] > item[0]:
                self.items.insert(i, item)
                tuple_insert = True
                break

        if not tuple_insert:
            self.items.append(item)

    def dequeue(self):
        # pop élément avec la plus haute priorité (normalement le premier de la file (voir enqueue))
        if not self.is_empty():
            return self.items.pop(0) 
        return None

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)
